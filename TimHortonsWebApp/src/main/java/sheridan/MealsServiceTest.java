package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		
		MealsService meals = new MealsService();
		List<String> mealsAvailable = meals.getAvailableMealTypes(MealType.DRINKS);
		
		assertTrue("No meals are available", !mealsAvailable.isEmpty());
	}

	@Test
	public void testDrinksException() {
		
		MealsService meals = new MealsService();
		List<String> mealsAvailable = meals.getAvailableMealTypes(null);
		assertTrue("Meals are available", mealsAvailable.get(0).equals("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		
		MealsService meals = new MealsService();
		List<String> mealsAvailable = meals.getAvailableMealTypes(MealType.DRINKS);
		
		assertTrue("Less than three meals are available", mealsAvailable.size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		
		MealsService meals = new MealsService();
		List<String> mealsAvailable = meals.getAvailableMealTypes(null);
		assertFalse("More than one option is recorded", mealsAvailable.size() > 1);
	}
}
